package com.testkotlin.func.utils

import com.aventstack.extentreports.ExtentReports
import com.aventstack.extentreports.ExtentTest
import com.aventstack.extentreports.reporter.ExtentHtmlReporter
import org.testng.ITestResult
import java.lang.reflect.Method

object ExtentReporter {
    private var extent: ExtentReports?= null
    private var parentTest: ExtentTest? = null
    var childTest: ExtentTest? = null

    fun initExtentReport() {
        extent = ExtentReports()
        val htmlReporter = ExtentHtmlReporter("extent.html")
        extent?.attachReporter(htmlReporter)
        extent?.setSystemInfo("OS", "Windows 10")
        extent?.setSystemInfo("Env", "Automated Testing")
    }

    fun createParentTest() {
        parentTest = extent?.createTest(javaClass.name)
    }

    fun createChildTest(method: Method) {
        childTest = parentTest?.createNode(method.name)
    }

    fun checkTestResult(result: ITestResult) {
        when {
            result.status == ITestResult.FAILURE -> childTest?.fail(result.throwable)
            result.status == ITestResult.SKIP -> childTest?.skip(result.throwable)
            else -> childTest?.pass("Test Passed!")
        }
        extent?.flush()
    }
}