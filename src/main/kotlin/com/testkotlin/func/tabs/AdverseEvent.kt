package com.testkotlin.func.tabs

import com.aventstack.extentreports.ExtentTest
import com.aventstack.extentreports.Status
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class AdverseEvent(private val driver: WebDriver, private val childTest: ExtentTest?) : BaseTab() {
    init {
        PageFactory.initElements(driver, this)
    }

    private val OUTCOME = "Resolved"
    private val INTENSITY = "Mild"
    private val RELATION = "Not Related"
    private val TEXT = "other"
    private val DATE = "1/1/1980"
    private val TIME = "12:00 AM"

    @FindBy(css = "#AdverseExperience_DesignIFrame")
    private val adverseExpField: WebElement? = null

    @FindBy(css = "#AdverseExperienceLabel")
    private val adverseExpFieldText: WebElement? = null

    @FindBy(css = "#OnsetDate_I")
    private val onsetDateField: WebElement? = null

    @FindBy(css = "#OnsetDate")
    private val onsetDateFieldText: WebElement? = null

    @FindBy(css = "#OnsetTime_I")
    private val onsetTimeField: WebElement? = null

    @FindBy(css = "#OnsetTime")
    private val onsetTimeFieldText: WebElement? = null

    @FindBy(css = "#EndDate_I")
    private val endDateField: WebElement? = null

    @FindBy(css = "#EndDate")
    private val endDateFieldText: WebElement? = null

    @FindBy(css = "#EndTime_I")
    private val endTimeField: WebElement? = null

    @FindBy(css = "#EndTime")
    private val endTimeFieldtext: WebElement? = null

    @FindBy(css = "#Outcome_B-1")
    private val outcomeButton: WebElement? = null

    @FindBy(css = "#Intensity_B-1")
    private val intensityButton: WebElement? = null

    @FindBy(css = "#RelationshipToInvestigationalDrug_B-1")
    private val relationButton: WebElement? = null

    @FindBy(css = "#btnSave1155_CD")
    private val saveButton: WebElement? = null

    @FindBy(css = "#Outcome_DDD_L_LBI1T0")
    private val outcomeField: WebElement? = null

    @FindBy(css = "#Outcome")
    private val outcomeFieldText: WebElement? = null

    @FindBy(css = "#Intensity_DDD_L_LBI1T0")
    private val intensityField: WebElement? = null

    @FindBy(css = "#Intensity")
    private val intensityFieldText: WebElement? = null

    @FindBy(css = "#RelationshipToInvestigationalDrug_DDD_L_LBI1T0")
    private val relationField: WebElement? = null

    @FindBy(css = "#RelationshipToInvestigationalDrug")
    private val relationFieldText: WebElement? = null

    @FindBy(css = ".addNewAE")
    private val aeTabHeader: WebElement? = null


    private fun switchToAdverseEvent() {
        childTest?.log(Status.INFO,"Переходим на вкладку Adverse Event")
        aeTabHeader?.click()
        waitAjaxProgress(driver)
    }

    private fun selectDropDown(button: WebElement?, field: WebElement?) {
        button?.click()
        field?.click()
    }

    private fun assertAdverseEventFields(exp: String, date: String, time: String) {
        childTest?.log(Status.INFO,"Проверяем значения")
        assert(adverseExpFieldText?.text == exp)
        assert(onsetDateFieldText?.text == date)
        assert(endDateFieldText?.text == date)
        assert(onsetTimeFieldText?.text == time)
        assert(endTimeFieldtext?.text == time)
        assert(outcomeFieldText?.text == OUTCOME)
        assert(intensityFieldText?.text == INTENSITY)
        assert(relationFieldText?.text == RELATION)
    }

    fun assertNewAdverseEventFields() {
        assertAdverseEventFields(TEXT, DATE, TIME)
    }

    fun createAdverseEventData() {
        switchToAdverseEvent()
        childTest?.log(Status.INFO,"Вводим значения в модуль Adverse Event")
        adverseExpField?.sendKeys(TEXT)
        onsetDateField?.sendKeys(DATE)
        onsetTimeField?.sendKeys(TIME)
        endDateField?.sendKeys(DATE)
        endTimeField?.sendKeys(TIME)
        selectDropDown(outcomeButton, outcomeField)
        selectDropDown(intensityButton, intensityField)
        selectDropDown(relationButton, relationField)
        saveButton?.click()
        waitAjaxProgress(driver)
    }



}