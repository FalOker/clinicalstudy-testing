package com.testkotlin.func.tabs

import com.aventstack.extentreports.ExtentTest
import com.aventstack.extentreports.Status
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import com.testkotlin.func.modules.Demographics
import com.testkotlin.func.modules.Vitals

class Baseline(private val driver: WebDriver, private val childTest: ExtentTest?) : BaseTab() {
    private val demo = Demographics(driver)
    private val vitals = Vitals(driver)

    init {
        PageFactory.initElements(driver, this)
    }

    @FindBy(css = "#PatientVisitsTabt_T1")
    private val baselineTabHeader: WebElement? = null

    fun editDemographicsData() {
        switchToBaseline()
        childTest?.log(Status.INFO,"Изменяем личные данные пациента")
        demo.dateOfBirthField?.sendKeys(demo.DATE)
        demo.otherInfoField?.sendKeys(demo.OTHER_INFO)
        demo.saveButton?.click()
        waitAjaxProgress(driver)
    }

    private fun switchToBaseline() {
        childTest?.log(Status.INFO,"Переходим на вкладку Baseline")
        baselineTabHeader?.click()
        waitAjaxProgress(driver)
    }

    fun editVitalsData() {
        switchToBaseline()
        childTest?.log(Status.INFO,"Изменяем данные о показаниях пациента")
        vitals.actualTimeField?.sendKeys(vitals.ACTUAL_TIME)
        vitals.heightField?.sendKeys(vitals.HEIGHT)
        vitals.weightField?.sendKeys(vitals.WEIGHT)
        vitals.tempField?.sendKeys(vitals.TEMP)
        vitals.heartRateField?.sendKeys(vitals.HEARTH_RATE)
        vitals.systolicField?.sendKeys(vitals.SYSTOLIC)
        vitals.diastolicField?.sendKeys(vitals.DIASTOLIC)
        vitals.saveButton?.click()
        waitAjaxProgress(driver)
    }

    private fun assertVitalsFields(
            actualTime: String,
            height: String,
            weight: String,
            temp: String,
            hearthRate: String,
            systolic: String,
            diastolic: String
    ) {
        childTest?.log(Status.INFO,"Проверяем данные о показаниях пациента")
        assert(vitals.actualTimeFieldText?.text == actualTime)
        assert(vitals.heightFieldText?.text == height)
        assert(vitals.weightFieldText?.text == weight)
        assert(vitals.tempFieldText?.text == temp)
        assert(vitals.heartRateFieldText?.text == hearthRate)
        assert(vitals.systolicFieldText?.text == systolic)
        assert(vitals.diastolicFieldText?.text == diastolic)
        assert(vitals.editButton!!.isDisplayed && vitals.editButton.isEnabled)
    }

    fun assertNewVitalsFields() {
        assertVitalsFields(
                vitals.ACTUAL_TIME,
                vitals.HEIGHT,
                vitals.WEIGHT,
                vitals.TEMP,
                vitals.HEARTH_RATE,
                vitals.SYSTOLIC,
                vitals.DIASTOLIC
        )
    }

    private fun assertDemographicsFields(date: String, other: String, gender: String, race: String) {
        childTest?.log(Status.INFO,"Проверяем личные данные")
        assert(demo.dateOfBirthFieldText?.text == date)
        assert(demo.otherInfoFieldText?.text == other)
        assert(demo.genderFieldText?.text == gender)
        assert(demo.raceFieldText?.text == race)
        assert(demo.editButton!!.isDisplayed && demo.editButton.isEnabled)
    }

    fun assertNewDemographicsFields() {
        assertDemographicsFields(demo.DATE, demo.OTHER_INFO, "Male", "White")
    }

    fun assertPatientPreview() {
        childTest?.log(Status.INFO,"Проверяем превью")
        assertPatientPreview("Subj A166", "Age 37", "Male")
    }
}