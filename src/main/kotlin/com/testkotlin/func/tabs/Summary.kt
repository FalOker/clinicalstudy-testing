package com.testkotlin.func.tabs

import com.aventstack.extentreports.ExtentTest
import com.aventstack.extentreports.Status
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

class Summary(private val driver: WebDriver, private val childTest: ExtentTest?) : BaseTab() {
    private val INITIALS = "AB"
    private val DATE = "1/1/2001"
    private val RAND_NUMBER = "1234"
    private val NEW_INITIALS = "BA"
    private val NEW_DATE = "2/1/2001"
    private val NEW_RAND_NUMBER = "4321"

    private val wait = WebDriverWait(driver, 10)
    private var patientName: String = ""

    @FindBy(css = "#btnNewPatient_CD > span:nth-child(2)")
    private val newPatientButton: WebElement? = null

    @FindBy(css = ".headerGroup > h1:nth-child(1)")
    private val newPatientTitle: WebElement? = null

    @FindBy(css = "#PatientInitials_I")
    private val patientInitialsField: WebElement? = null

    @FindBy(css = "#PatientInitialsLabel")
    private val patientInitialsFieldText: WebElement? = null

    @FindBy(css = "div.editor-field:nth-child(4) > span:nth-child(4)")
    private val patientNumber: WebElement? = null

    @FindBy(css = "#IsActive_S_D")
    private val activeCheckBox: WebElement? = null

    @FindBy(css = "#IsActive")
    private val activeCheckBoxText: WebElement? = null

    @FindBy(css = "#IsEnrolled_S_D")
    private val enrolledCheckBox: WebElement? = null

    @FindBy(css = "#IsEnrolled")
    private val enrolledCheckBoxText: WebElement? = null

    @FindBy(css = "#EnrollDate_I")
    private val enrollDateField: WebElement? = null

    @FindBy(css = "#EnrollDate")
    private val enrollDateFieldText: WebElement? = null

    @FindBy(css = "#RandomisationNumber_I")
    private val randNumberField: WebElement? = null

    @FindBy(css = "#RandomisationNumber")
    private val randNumberFieldText: WebElement? = null

    @FindBy(css = "#RandomisationDate_I")
    private val randDateField: WebElement? = null

    @FindBy(css = "#RandomisationDate")
    private val randDateFieldText: WebElement? = null

    @FindBy(css = "#HeaderPatientInitialsLabel")
    private val patientInitHeader: WebElement? = null

    @FindBy(css = "#btnSave_CD > span:nth-child(2)")
    private val saveButton: WebElement? = null

    @FindBy(css = "#btnEditPatient_CD")
    private val editButton: WebElement? = null

    init {
        PageFactory.initElements(driver, this)
    }

    fun createPatient() {
        newPatientButton?.click()
        waitAjaxProgress(driver)
        childTest?.log(Status.INFO, "Присутствует тайтл с текстом 'New Patient'")
        assert(newPatientTitle?.text == "New Patient")
        childTest?.log(Status.INFO, "Ввести все значения, чекбоксы не менять")
        patientInitialsField?.sendKeys(INITIALS)
        enrollDateField?.sendKeys(DATE)
        randNumberField?.sendKeys(RAND_NUMBER)
        randDateField?.sendKeys(DATE)
        wait.until(ExpectedConditions.elementToBeClickable(saveButton))
        saveButton?.click()
        waitAjaxProgress(driver)
    }

    private fun assertFields(activeCheckBox: String,
                             enrolledCheckBox: String,
                             initials: String,
                             enrollDate: String,
                             randNumber: String,
                             randDate: String) {
        childTest?.log(Status.INFO,"Обновляем браузер, чтобы обновился хэдер")
        driver.navigate().refresh()
        waitAjaxProgress(driver)
        childTest?.log(Status.INFO,"Проверяем все значения и кнопку Edit")
        patientName = "Subj A" + patientNumber?.text
        childTest?.log(Status.INFO,"ID пациента $patientName")
        assert(newPatientTitle?.text == patientName)
        assert(patientInitHeader?.text == initials)
        assert(patientInitialsFieldText?.text == initials)
        assert(activeCheckBoxText?.text == activeCheckBox)
        assert(enrolledCheckBoxText?.text == enrolledCheckBox)
        assert(enrollDateFieldText?.text == enrollDate)
        assert(randNumberFieldText?.text == randNumber)
        assert(randDateFieldText?.text == randDate)
        assert(editButton!!.isDisplayed && editButton.isEnabled)
    }

    fun assertFields() {
        assertFields("Yes", "No", INITIALS, DATE, RAND_NUMBER, DATE)
    }

    fun assertNewFields() {
        assertFields("No", "Yes", NEW_INITIALS, NEW_DATE, NEW_RAND_NUMBER, NEW_DATE)
    }

    fun login() {
        loginAsDoctor(driver, childTest)
    }

    fun assertPatientPreview() {
        childTest?.log(Status.INFO,"Проверяем превью")
        assertPatientPreview(patientName, "Unknown", "Unknown")
    }

    fun changePatientData() {
        childTest?.log(Status.INFO,"Изменяем данные пациента пациента")
        editButton?.click()
        waitAjaxProgress(driver)
        activeCheckBox?.click()
        enrolledCheckBox?.click()
        patientInitialsField?.clear()
        patientInitialsField?.sendKeys(NEW_INITIALS)
        enrollDateField?.clear()
        enrollDateField?.sendKeys(NEW_DATE)
        randNumberField?.clear()
        randNumberField?.sendKeys(NEW_RAND_NUMBER)
        randDateField?.clear()
        randDateField?.sendKeys(NEW_DATE)
        wait.until(ExpectedConditions.elementToBeClickable(saveButton))
        saveButton?.click()
        waitAjaxProgress(driver)
    }

    fun assertStatusChange() {
        childTest?.log(Status.INFO,"Проверяем что превью пациента перенесена в таб inactive")
        assertStatusChange(patientName, driver)
    }
}