package com.testkotlin.func.tabs

import com.aventstack.extentreports.ExtentTest
import com.aventstack.extentreports.Status
import com.testkotlin.func.modules.Inventory
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class TenthDay(private val driver: WebDriver, private val childTest: ExtentTest?) : BaseTab() {
    private val inventory = Inventory(driver)

    init {
        PageFactory.initElements(driver, this)
    }

    @FindBy(css = "#PatientVisitsTabt_T3")
    private val tenthDayTab: WebElement? = null

    private fun switchToTenthDay() {
        childTest?.log(Status.INFO,"Перейти на вкладку десятый день")
        tenthDayTab?.click()
        waitAjaxProgress(driver)
    }

    private fun addNewMedicationUsageRow(quantity: String, newButton: WebElement?) {
        childTest?.log(Status.INFO,"Добавляем новый рецепт в табличку")
        newButton?.click()
        waitAjaxProgress(driver)
        inventory.dateUsedField?.sendKeys(inventory.DATE)
        inventory.quantityUsedField?.sendKeys(quantity)
        inventory.updateButton?.click()
        waitAjaxProgress(driver)
    }

    private fun assertInventoryFields(quantity: String, batch: String, date: String) {
        childTest?.log(Status.INFO,"Проверям данные вне таблички")
        assert(inventory.quantityFieldText?.text == quantity)
        assert(inventory.batchFieldText?.text == batch)
        assert(inventory.receiptDateFieldText?.text == date)
        assert(inventory.shipDateFieldText?.text == date)
    }

    private fun assertMedicationUsageRow(dateField: WebElement?, date: String, quantityElementField: WebElement?, quantity: String) {
        childTest?.log(Status.INFO,"Проверяем новую строку в табличке")
        assert(dateField?.text == date)
        assert(quantityElementField?.text == quantity)
    }

    fun assertNewInventoryFields() {
        assertInventoryFields(inventory.QUANTITY_SHIPPED, inventory.BATCH, inventory.DATE)
    }

    fun editInventotyData() {
        switchToTenthDay()
        childTest?.log(Status.INFO,"Изменяем выписки рецептов")
        inventory.quantityField?.sendKeys(inventory.QUANTITY_SHIPPED)
        inventory.batchField?.sendKeys(inventory.BATCH)
        inventory.receiptDateField?.sendKeys(inventory.DATE)
        inventory.shipDateField?.sendKeys(inventory.DATE)
        addNewMedicationUsageRow(inventory.QUANTITY_USED_0, inventory.newButton)
        assertMedicationUsageRow(inventory.dateUsedFieldText, inventory.DATE, inventory.quantityUsedFieldText, inventory.QUANTITY_USED_0)
        addNewMedicationUsageRow(inventory.QUANTITY_USED_1, inventory.secondNewButton)
        assertMedicationUsageRow(inventory.secondDateUsedFieldText, inventory.DATE, inventory.secondQuantityUsedFieldText, inventory.QUANTITY_USED_1)
        inventory.saveButton?.click()
        waitAjaxProgress(driver)
    }

}