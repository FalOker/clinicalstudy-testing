package com.testkotlin.func.tabs

import com.aventstack.extentreports.ExtentTest
import com.aventstack.extentreports.Status
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.Assert
import org.testng.Reporter

abstract class BaseTab {
    @FindBy(className = "dx-vam")
    private val submitButton: WebElement? = null

    @FindBy(id = "uc_dmitchell")
    private val doctorIcon: WebElement? = null

    @FindBy(css = ".title")
    private val helloTitle: WebElement? = null

    @FindBy(css = "#PatientListGrid_tccell0_0 > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > span:nth-child(1)")
    private val patientSubject: WebElement? = null

    @FindBy(css = "#PatientListGrid_tccell0_0 > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > span:nth-child(2)")
    private val patientGender: WebElement? = null

    @FindBy(css = "#PatientListGrid_tccell0_0 > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > span:nth-child(3)")
    private val patientAge: WebElement? = null

    @FindBy(css = "#navigationLeftTab_T1Img")
    private val inactiveTab: WebElement? = null

    @FindBy(css = "#PatientListGrid_tccell0_0 > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > span:nth-child(1)")
    private val inactiveTabPatientName: WebElement? = null

    fun assert(condition: Boolean) {
        Assert.assertTrue(condition)
    }

    fun waitAjaxProgress(driver: WebDriver) {
        WebDriverWait(driver, 10).until { drv: WebDriver ->
            val js = drv as JavascriptExecutor
            js.executeScript("return jQuery.active == 0") as Boolean
        }
    }

    fun loginAsDoctor(driver: WebDriver, childTest: ExtentTest?) {
        childTest?.log(Status.INFO, "Перейти на страницу https://demos.devexpress.com/RWA/ClinicalStudy/DataCapture/Dashboard")
        driver.get("https://demos.devexpress.com/RWA/ClinicalStudy/DataCapture/Dashboard")
        val wait = WebDriverWait(driver, 10)
        waitAjaxProgress(driver)
        childTest?.log(Status.INFO, "Залогиниться Доктором Дэвидом Митчелом")
        doctorIcon?.click()
        wait.until(ExpectedConditions.elementToBeClickable(submitButton))
        submitButton?.click()
        waitAjaxProgress(driver)
        childTest?.log(Status.INFO,"Отображается фраза 'Hello, Dr. Mitchell'")
        wait.until(ExpectedConditions.elementToBeClickable(helloTitle))
        assert(helloTitle?.text == "Hello, Dr. Mitchell")
    }

    fun assertPatientPreview(expectedName: String, expectedAge: String, expectedGender: String) {
        assert(patientSubject?.text == expectedName)
        assert(patientGender?.text == expectedGender)
        assert(patientAge?.text == expectedAge)
    }

    fun assertStatusChange(expectedName: String, driver: WebDriver) {
        assert(patientSubject?.text != expectedName)
        inactiveTab?.click()
        waitAjaxProgress(driver)
        Thread.sleep(4000)
        assert(inactiveTabPatientName?.text == expectedName)
    }


}
