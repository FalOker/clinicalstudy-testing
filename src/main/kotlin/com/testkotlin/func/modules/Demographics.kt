package com.testkotlin.func.modules

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class Demographics(driver: WebDriver) {
    init {
        PageFactory.initElements(driver, this)
    }

    val DATE = "1/1/1980"
    val OTHER_INFO = "other info"

    @FindBy(css = "#DateOfBirth_I")
    val dateOfBirthField: WebElement? = null

    @FindBy(css = "#Other_I")
    val otherInfoField: WebElement? = null

    @FindBy(css = "#btnSave1147_CD")
    val saveButton: WebElement? = null

    @FindBy(css = "#DateOfBirth")
    val dateOfBirthFieldText: WebElement? = null

    @FindBy(css = "#Race")
    val raceFieldText: WebElement? = null

    @FindBy(css = "#Sex")
    val genderFieldText: WebElement? = null

    @FindBy(css = "#Other")
    val otherInfoFieldText: WebElement? = null

    @FindBy(css = "#btnEditDemog_CD")
    val editButton: WebElement? = null
}