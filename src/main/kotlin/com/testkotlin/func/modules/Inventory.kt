package com.testkotlin.func.modules

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class Inventory(driver: WebDriver) {
    init {
        PageFactory.initElements(driver, this)
    }

    val QUANTITY_SHIPPED = "0.5"
    val BATCH = "1"
    val DATE = "1/1/1980"
    val QUANTITY_USED_0 = "2"
    val QUANTITY_USED_1 = "1"

    @FindBy(css = "#QuantityShipped_I")
    val quantityField: WebElement? = null

    @FindBy(css = "#QuantityShipped")
    val quantityFieldText: WebElement? = null

    @FindBy(css = "#BatchNumber_I")
    val batchField: WebElement? = null

    @FindBy(css = "#BatchNumber")
    val batchFieldText: WebElement? = null

    @FindBy(css = "#ReceiptDate_I")
    val receiptDateField: WebElement? = null

    @FindBy(css = "#ReceiptDate")
    val receiptDateFieldText: WebElement? = null

    @FindBy(css = "#ShipDate_I")
    val shipDateField: WebElement? = null

    @FindBy(css = "#ShipDate")
    val shipDateFieldText: WebElement? = null

    @FindBy(css = "#btnSave1153_CD")
    val saveButton: WebElement? = null

    @FindBy(css = "#gvRepeatableInventory_DXCBtn0")
    val newButton: WebElement? = null

    @FindBy(css = "#gvRepeatableInventory_DXEditor1_I")
    val dateUsedField: WebElement? = null

    @FindBy(css = "#gvRepeatableInventory_DXEditor2_I")
    val quantityUsedField: WebElement? = null

    @FindBy(css = "#gvRepeatableInventory_DXCBtn0")
    val updateButton: WebElement? = null

    @FindBy(css = "td.dxgv:nth-child(2)")
    val dateUsedFieldText: WebElement? = null

    @FindBy(css = "td.dxgv:nth-child(3)")
    val quantityUsedFieldText: WebElement? = null

    @FindBy(css = "#gvRepeatableInventory_DXCBtn1")
    val secondNewButton: WebElement? = null

    @FindBy(css = "#gvRepeatableInventory_DXDataRow1 > td:nth-child(2)")
    val secondDateUsedFieldText: WebElement? = null

    @FindBy(css = "#gvRepeatableInventory_DXDataRow1 > td:nth-child(3)")
    val secondQuantityUsedFieldText: WebElement? = null



}