package com.testkotlin.func.modules

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class Vitals(driver: WebDriver) {
    init {
        PageFactory.initElements(driver, this)
    }

    val ACTUAL_TIME = "11:00 AM"
    val HEIGHT = "100"
    val WEIGHT = "100"
    val TEMP = "36"
    val HEARTH_RATE = "100"
    val SYSTOLIC = "100"
    val DIASTOLIC = "100"


    @FindBy(css = "#ActualTime_I")
     val actualTimeField: WebElement? = null

    @FindBy(css = "#ActualTime")
     val actualTimeFieldText: WebElement? = null

    @FindBy(css = "#Height_I")
     val heightField: WebElement? = null

    @FindBy(css = "#Height")
     val heightFieldText: WebElement? = null

    @FindBy(css = "#Weight_I")
     val weightField: WebElement? = null

    @FindBy(css = "#Weight")
     val weightFieldText: WebElement? = null

    @FindBy(css = "#Temperature_I")
     val tempField: WebElement? = null

    @FindBy(css = "#Temperature")
     val tempFieldText: WebElement? = null

    @FindBy(css = "#HeartRate_I")
     val heartRateField: WebElement? = null

    @FindBy(css = "#HeartRate")
     val heartRateFieldText: WebElement? = null

    @FindBy(css = "#BloodPressureSystolic_I")
     val systolicField: WebElement? = null

    @FindBy(css = "#BloodPressureSystolic")
     val systolicFieldText: WebElement? = null

    @FindBy(css = "#BloodPressureDiastolic_I")
     val diastolicField: WebElement? = null

    @FindBy(css = "#BloodPressureDiastolic")
     val diastolicFieldText: WebElement? = null

    @FindBy(css = "#btnSave1148_CD")
     val saveButton: WebElement? = null

    @FindBy(css = "#btnEditVitals_CD")
     val editButton: WebElement? = null
}