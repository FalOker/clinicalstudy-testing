package com.testkotlin.test

import com.aventstack.extentreports.ExtentReports
import com.aventstack.extentreports.ExtentTest
import com.aventstack.extentreports.reporter.ExtentHtmlReporter
import com.testkotlin.func.utils.ExtentReporter
import org.openqa.selenium.WebDriver
import org.openqa.selenium.firefox.FirefoxDriver
//import org.openqa.selenium.htmlunit
import org.testng.annotations.*
import org.testng.annotations.BeforeMethod
import org.testng.annotations.AfterMethod
import java.lang.reflect.Method
import org.testng.ITestResult

open class BaseTest {
    lateinit var driver: WebDriver

    @BeforeSuite
    fun beforeSuite() {
        ExtentReporter.initExtentReport()
    }

    @BeforeClass
    fun beforeClass() {
        ExtentReporter.createParentTest()
    }

    @BeforeMethod
    fun beforeMethod(method: Method) {
        ExtentReporter.createChildTest(method)
    }

    @BeforeMethod(alwaysRun = true)
    fun setup() {
        //TODO change to HtmlUnit
        System.setProperty("webdriver.gecko.driver", "/home/user/geckodriver")
        driver = FirefoxDriver()
        driver.manage()?.window()?.maximize()
    }

    @AfterMethod(alwaysRun = true)
    fun after(result: ITestResult) {
        ExtentReporter.checkTestResult(result)
        driver.close()
    }

}


