package com.testkotlin.test

import com.aventstack.extentreports.ExtentReports
import com.aventstack.extentreports.reporter.ExtentHtmlReporter
import com.testkotlin.func.tabs.AdverseEvent
import com.testkotlin.func.tabs.Baseline
import com.testkotlin.func.tabs.Summary
import com.testkotlin.func.tabs.TenthDay
import com.testkotlin.func.utils.ExtentReporter
import com.testkotlin.func.utils.ExtentReporter.childTest
import org.testng.annotations.BeforeClass
import org.testng.annotations.BeforeSuite
import org.testng.annotations.BeforeTest
import org.testng.annotations.Test




class EditPatientTestSuite : BaseTest(){
//    @BeforeSuite(alwaysRun = true)
//    fun beforesuite() {
//        extent = ExtentReports()
//        val htmlReporter = ExtentHtmlReporter("extent.html")
//        extent.attachReporter(htmlReporter)
//        extent.setSystemInfo("OS", "Windows 10")
//        extent.setSystemInfo("Env", "Automated Testing")
//    }

//    @BeforeTest(alwaysRun = true)
//    fun setupTestSuite() {
//        parentTest = extent!!.createTest(javaClass.name)
//    }

    private fun createPatient() {
        val summaryTab = Summary(driver, childTest)
        summaryTab.login()
        summaryTab.createPatient()
    }

//    @Test(description = "Создание пациента")
//    fun testCreatePatient() {
//        val summaryTab = Summary(driver, childTest)
//
//        summaryTab.login()
//        summaryTab.createPatient()
//        summaryTab.assertFields()
//        summaryTab.assertPatientPreview()
//    }


    @Test(description = "Изменение данных во вкладке summary")
    fun testEditSummaryTab() {
        val summaryTab = Summary(driver, childTest)
        summaryTab.login()
        summaryTab.createPatient()
        summaryTab.changePatientData()
        summaryTab.assertNewFields()
        summaryTab.assertStatusChange()
    }

//    @Test(description = "Изменение данных пациента в модуле Demographics")
//    fun testEditPatientDemographics() {
//        val baselineTab = Baseline(driver, childTest)
//
//        createPatient()
//        baselineTab.editDemographicsData()
//        baselineTab.assertNewDemographicsFields()
//        baselineTab.assertPatientPreview()
//    }
//
//    @Test(description = "Изменение данных пациента в модуле Vitals")
//    fun testEditPatientVitals() {
//        val baselineTab = Baseline(driver, childTest)
//
//        createPatient()
//        baselineTab.editVitalsData()
//        baselineTab.assertNewVitalsFields()
//    }
//
//    @Test(description = "Добавление нескольких рецептов в таблицу Medication Usage")
//    fun testAddRowsInMedicationUsage() {
//        val tenthDayTab = TenthDay(driver, childTest)
//
//        createPatient()
//        tenthDayTab.editInventotyData()
//        tenthDayTab.assertNewInventoryFields()
//    }
//
//    @Test(description = "Создание новой вкладки Adverse Event")
//    fun testCreateAdverseEventTab() {
//        val adverseEventTab = AdverseEvent(driver, childTest)
//
//        createPatient()
//        adverseEventTab.createAdverseEventData()
//        adverseEventTab.assertNewAdverseEventFields()
//    }
}




