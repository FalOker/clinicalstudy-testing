package com.testkotlin.test

import com.aventstack.extentreports.ExtentReports
import com.aventstack.extentreports.reporter.ExtentHtmlReporter
import com.testkotlin.func.tabs.Summary
import com.testkotlin.func.utils.ExtentReporter.childTest
import org.testng.annotations.BeforeSuite
import org.testng.annotations.BeforeTest
import org.testng.annotations.Test

class CreatePatientTestSuite : BaseTest() {
//    @BeforeSuite(alwaysRun = true)
//    fun beforesuite() {
//        extent = ExtentReports()
//        val htmlReporter = ExtentHtmlReporter("extent.html")
//        extent.attachReporter(htmlReporter)
//        extent.setSystemInfo("OS", "Windows 10")
//        extent.setSystemInfo("Env", "Automated Testing")
//    }

//    @BeforeTest(alwaysRun = true)
//    fun setupTestSuite() {
//        parentTest = extent!!.createTest(javaClass.name)
//    }
//    @BeforeTest(alwaysRun = true)
//    fun setupTestSuite() {
//        parentTest = extent!!.createTest(javaClass.name)
//    }

    @Test(description = "Создание пациента")
    fun testCreatePatient() {
        val summaryTab = Summary(driver, childTest)
        summaryTab.login()
        summaryTab.createPatient()
        summaryTab.assertFields()
        summaryTab.assertPatientPreview()
    }
}